const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/poly-bot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}).then(res => {
    console.log('DB started')
}).catch(err => {
    console.log('DB not started: ' + err)
})