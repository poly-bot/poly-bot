const path = require('path')
const fs = require('fs')
const Discord = require('discord.js')
const Joke = require('awesome-dev-jokes')
const cows = require('cows')
const { Aki } = require('aki-api');
const Bot = require('../api/bot')
const TicTacToe = require('../api/ticTacToe')
const Meme = require('../api/meme')
const Scream = require('../api/scream')
const Demineur = require('../api/demineur')

const actions = new Bot.Actions()

actions.addAction('joke', (message, params, events) => {
    const embed = new Discord.MessageEmbed()
    embed.setDescription(Joke.getRandomJoke())
    return embed
})

actions.addAction('meme', (message, params) => {
    return new Promise((resolve, reject) => {
        Meme.fetchMeme()
            .then(meme => {
                const embed = new Discord.MessageEmbed()
                embed.setTitle(meme.title)
                embed.setImage(meme.img)
                resolve(embed)
            })
            .catch(e => reject(e))
    })
    
})

const rpc = (message, params) => {
    const EMOJI = ['🪨', '📜', '✂️']
    const botPlay = EMOJI[Math.floor(Math.random() * EMOJI.length)]
    var played = null
    
    const WIN = 0
    const LOOSE = 1
    const DRAW = 2

    const embed = new Discord.MessageEmbed()
    embed.setTitle('Roche papier ciseau')
    embed.setDescription('Fait ton choix')
    message.channel.send(embed)
        .then(async (sent) => {
            try {
                for(const e in EMOJI) {
                    await sent.react(EMOJI[e])
                }

                const filter = (reaction, user) => {
                    return EMOJI.includes(reaction.emoji.name) && user.id === message.author.id;
                }
        
                const collector = sent.createReactionCollector(filter, { time: 90 * 1000, limit: 1 });

                var result = null
        
                collector.on('collect', (reaction, user) => {
                    played = reaction.emoji.name
                    if(botPlay == '🪨') {
                        if(played == '🪨') result = DRAW
                        if(played == '📜') result = WIN
                        if(played == '✂️') result = LOOSE
                    }
                    if(botPlay == '📜') {
                        if(played == '🪨') result = LOOSE
                        if(played == '📜') result = DRAW
                        if(played == '✂️') result = WIN
                    }
                    if(botPlay == '✂️') {
                        if(played == '🪨') result = WIN
                        if(played == '📜') result = LOOSE
                        if(played == '✂️') result = DRAW
                    }
                    collector.stop()
                })

                collector.on('end', () => {
                    const embed = new Discord.MessageEmbed()
                    embed.setTitle('Roche papier ciseau')
                    if(result == WIN)
                        embed.setDescription(played + ' vs ' + botPlay + '. Vous avez gagné!')
                    else if(result == LOOSE)
                        embed.setDescription(played + ' vs ' + botPlay + '. Vous avez perdu.')
                    else if(result == DRAW)
                        embed.setDescription(played + ' vs ' + botPlay + '. Personne n\'a gagné')
                    else 
                        embed.setDescription('Partie terminée')
                    sent.edit(embed)
                })
            } catch(e) {
        
            }
        })
        .catch(e => console.log(e))
}
actions.addAction('roche papier ciseau', rpc)
actions.addAction('rpc', rpc)

actions.addAction('tic tac toe', (message, params, events) => {
    const maxTime = 60 // in seconds
    const ticTacToe = new TicTacToe()
    // ticTacToe.difficulty = ticTacToe.HARD

    events.on('sent', async (sentMessage) => {
        const filter = (reaction, user) => {
            // return user.id === message.author.id
            return Object.keys(ticTacToe.emoji).includes(reaction.emoji.name) && user.id === message.author.id;
        }

        const collector = sentMessage.createReactionCollector(filter, { time: maxTime * 1000 });

        collector.on('collect', (reaction, user) => {
            const played = ticTacToe.play(ticTacToe.emoji[reaction.emoji.name])
            if(!played) return

            if(ticTacToe.getWinner() || ticTacToe.boardIsFull()) {
                collector.stop()
            } else {
                ticTacToe.playBot()

                if(ticTacToe.getWinner() || ticTacToe.boardIsFull()) {
                    collector.stop()
                    return
                }

                const embed = new Discord.MessageEmbed()
                embed.setTitle('Tic Tac Toe: ' + ticTacToe.getDifficultyStr(ticTacToe.difficulty)) 
                embed.setDescription(ticTacToe.getString())

                sentMessage.edit(embed)
                    .then(() => {
                        ticTacToe.turn = ticTacToe.USER
                    })
            }
        })

        collector.on('end', collected => {
            const embed = new Discord.MessageEmbed()

            embed.setTitle('Tic Tac Toe: ' + ticTacToe.getDifficultyStr(ticTacToe.difficulty))
            embed.setDescription(ticTacToe.getString())
            embed.setFooter('Partie terminée: ' + ticTacToe.getWinnerStr())

            sentMessage.edit(embed)
        })

        for(const e of Object.keys(ticTacToe.emoji)) {
            try {
                await sentMessage.react(e)
            } catch(e) {}
        }
    })

    const embed = new Discord.MessageEmbed()
    embed.setTitle('Tic Tac Toe: ' + ticTacToe.getDifficultyStr(ticTacToe.difficulty))
    embed.setDescription(ticTacToe.getString())
    return embed
})

actions.addAction('tic tac toe :difficulty', (message, params, events) => {
    const maxTime = 90 // in seconds
    const ticTacToe = new TicTacToe()
    ticTacToe.difficulty = ticTacToe.getDifficulty(params.difficulty)

    console.log(params.difficulty, ticTacToe.difficulty)

    console.log(ticTacToe.EASY, ticTacToe.MEDIUM, ticTacToe.HARD, ticTacToe.IMPOSSIBLE)

    if(ticTacToe.difficulty == null) {
        ticTacToe.difficulty = ticTacToe.IMPOSSIBLE
        console.log(ticTacToe.difficulty)
        const embed = new Discord.MessageEmbed()
        embed.setDescription('Difficulté `' + params.difficulty + '` invalide. La partie sera `' + ticTacToe.getDifficultyStr(ticTacToe.difficulty) + '` par défaut.')
        embed.setFooter('Veuillez choisir parmis: ' + ticTacToe.getDifficultiesStr())
        message.channel.send(embed)
    }

    events.on('sent', async (sentMessage) => {
        const filter = (reaction, user) => {
            // return user.id === message.author.id
            return Object.keys(ticTacToe.emoji).includes(reaction.emoji.name) && user.id === message.author.id;
        }

        const collector = sentMessage.createReactionCollector(filter, { time: maxTime * 1000 });

        collector.on('collect', (reaction, user) => {
            const played = ticTacToe.play(ticTacToe.emoji[reaction.emoji.name])
            if(!played) return
 
            if(ticTacToe.getWinner() || ticTacToe.boardIsFull()) {
                collector.stop()
            } else {
                ticTacToe.playBot()

                if(ticTacToe.getWinner() || ticTacToe.boardIsFull()) {
                    collector.stop()
                    return
                }

                const embed = new Discord.MessageEmbed()
                embed.setTitle('Tic Tac Toe: ' + ticTacToe.getDifficultyStr(ticTacToe.difficulty))
                embed.setDescription(ticTacToe.getString())

                sentMessage.edit(embed)
                    .then(() => {
                        ticTacToe.turn = ticTacToe.USER
                    })
            }
        })

        collector.on('end', collected => {
            const embed = new Discord.MessageEmbed()

            embed.setTitle('Tic Tac Toe: ' + ticTacToe.getDifficultyStr(ticTacToe.difficulty))
            embed.setDescription(ticTacToe.getString())
            embed.setFooter('Partie terminée: ' + ticTacToe.getWinnerStr())

            sentMessage.edit(embed)
        })

        for(const e of Object.keys(ticTacToe.emoji)) {
            try {
                await sentMessage.react(e)
            } catch(e) {}
        }
    })

    const embed = new Discord.MessageEmbed()
    embed.setTitle('Tic Tac Toe: ' + ticTacToe.getDifficultyStr(ticTacToe.difficulty))
    embed.setDescription(ticTacToe.getString())
    return embed
})

const cowAction = (message, params, events) => {
    const c = cows()
    const index = Math.floor(Math.random() * c.length)

    const embed = new Discord.MessageEmbed()
    embed.setDescription('```' + c[index] + '```')
    return embed
}
actions.addAction('cow', cowAction)
for(var i = 1; i <= 20; ++i) {
    var str = 'me'
    for(var j = 0; j < i; ++j) {
        str += 'u'
    }
    actions.addAction(str, cowAction)
    actions.addAction(str + 'h', cowAction)
}

const tokebakicite = (message, params, events) => {
    const items = fs.readFileSync(path.join(__dirname, '../data/ascii/quebec.txt'), 'utf8').split('\n')
    const item = items[Math.floor(Math.random() * items.length)].replace(/\\n/g, '\n')

    const embed = new Discord.MessageEmbed()
    embed.setDescription('```' + item + '```')
    return embed
}
actions.addAction('tokebakicite', tokebakicite)
actions.addAction('tokebakicitte', tokebakicite)
actions.addAction('tokebekicite', tokebakicite)
actions.addAction('tokebekicitte', tokebakicite)
actions.addAction('awaille kevin', tokebakicite)
actions.addAction('awaille kevin osti continue comme ço', tokebakicite)

const scream = (message, params, events, bot) => {
    return new Promise((resolve, reject) => {
        const voiceChannel = message.member.voice.channel

        Scream.getRandomScream()
            .then(async (scream) => {
                const embed = new Discord.MessageEmbed()

                embed.setTitle('SCREAM')
                embed.setDescription('Joue le cri dans ' + voiceChannel.name)

                if(!voiceChannel) embed.setDescription('Connectez-vous à un channel de voix pour faire jouer un cri.')

                resolve(embed)

                if(voiceChannel) {
                    try {
                        const connection = await voiceChannel.join()

                        const dispatcher = connection.play(scream.fileUrl)
                        bot.dispatchers.push(dispatcher)
                        
                        const now = Date.now()
                        bot.lastPlayed = now

                        dispatcher.on('finish', () => {
                            setTimeout(() => {
                                if(bot.lastPlayed == now) {
                                    connection.disconnect()
                                }
                            }, 2 * 60 * 1000)
                        })

                        dispatcher.on('error', console.error)
                    } catch(e) {
                        console.error('Can\'t play audio', e)
                    }
                }
            })
            .catch(err => {
                const embed = new Discord.MessageEmbed()
                embed.setTitle('SCREAM')
                embed.setDescription('Un erreur est parvenu. Impossible de trouver un cri.')
                resolve(embed)
                console.error(err)
            })
    })
}
actions.addAction('scream', scream)
for(var i = 2; i <= 20; ++i) {
    var str = 'h'
    for(var j = 0; j < i; ++j) {
        str += 'a'
    }
    actions.addAction(str, scream)
}

const homeland = (message, params, events, bot) => {
    return new Promise(async (resolve, reject) => {
        const voiceChannel = message.member.voice.channel
        const image = 'https://cdn.britannica.com/36/22536-004-9855C103/Flag-Union-of-Soviet-Socialist-Republics.jpg'

        if(voiceChannel) {
            try {
                const embed = new Discord.MessageEmbed()
                embed.setTitle('Homeland')
                embed.setImage(image)
                resolve(embed)

                const connection = await voiceChannel.join()

                const dispatcher = connection.play(fs.createReadStream(path.join(__dirname, '../data/sound/urss.mp3')))
                bot.dispatchers.push(dispatcher)
                            
                const now = Date.now()
                bot.lastPlayed = now
    
                dispatcher.on('finish', () => {
                    setTimeout(() => {
                        if(bot.lastPlayed == now) {
                            connection.disconnect()
                        }
                    }, 2 * 60 * 1000)
                })
    
                dispatcher.on('error', console.error)
            } catch(err) {
                const embed = new Discord.MessageEmbed()
                embed.setTitle('Homeland')
                embed.setImage(image)
                embed.setDescription('Un erreur est parvenu. Impossible de trouver un cri.')
                resolve(embed)
                console.error(err)
            }
            
        } else {
            const embed = new Discord.MessageEmbed()
            embed.setTitle('Homeland')
            embed.setImage(image)
            embed.setDescription('Veuillez joindre un voice channel.')
        }
    })
}
actions.addAction('homeland', homeland)
actions.addAction('daddy stalin', homeland) 

const pop = (message, params, events, bot) => {
    const STR = 'POP'
    const SURPRISE = '  :gift: '
    const Y = 20
    const X = 8

    var str = ''
    const y = Math.floor(Math.random() * Y)
    const x = Math.floor(Math.random() * X)

    for(var i = 0; i < Y; ++i) {
        for(var j = 0; j < X; ++j) {
            if(i == y && j == x) {
                str += '||' + SURPRISE + '||'
            } else {
                str += '||' + STR + '||'
            }
        }
        str += '\n'
    }

    const embed = new Discord.MessageEmbed()
    embed.setTitle('Papier bulle')
    embed.setDescription(str)
    return embed
}
actions.addAction('papier bulle', pop)
for(var i = 1; i <= 20; ++i) {
    var str = ''
    for(var j = 0; j < i; ++j) {
        str += 'pop '
    }
    str = str.trim()
    actions.addAction(str, pop)
}

const demineur = (message, params, event, bot) => {
    var difficulty = params.difficulty || 'MEDIUM'
    difficulty = difficulty.toUpperCase()
    const demineur = new Demineur(difficulty)

    const embed = new Discord.MessageEmbed()
    embed.setTitle('Démineur: ' + difficulty.toLowerCase())

    var str = ''
    demineur.board.forEach(line => {
        line.forEach(c => {
            str += '||` ' + c + ' `||'
        })
        str += '\n'
    })
    embed.setDescription(str)

    return embed
}
actions.addAction('demineur', demineur)
actions.addAction('demineur :difficulty', demineur)
actions.addAction('démineur', demineur)
actions.addAction('démineur :difficulty', demineur)

// actions.addAction('akinator', (message, params, events) => {
//     const aki = new Aki('fr');
//     const CHOICE = '1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣🔟'.split('')

//     const embed = new Discord.MessageEmbed()
//     embed.setTitle('Akinator')
//     embed.setDescription('Démarage de la partie')
//     message.channel.send(embed)
//         .then(async (sent) => {
//             await aki.start();
//             console.log(aki)

//             const filter = (reaction, user) => {
//                 return  user.id === message.author.id;
//             }
//             const collector = sent.createReactionCollector(filter, { time: 200 * 1000 });

//             collector.on('collect', (reaction, user) => {
//                 const played = reaction.emoji.name
//                 console.log(played)
//                 sent.reactions.get(played).remove(user)
//             })

//             collector.on('end', collected => {
                
//             })

//             const embed = new Discord.MessageEmbed()
//             embed.setTitle('Akinator')
//             var test = aki.question + '\n'
//             aki.answers.forEach((a, i) => {
//                 test += '\n' + CHOICE[i] + ' ' + a
//             })
//             embed.setDescription(test)
//             embed.setFooter(aki.progress + '%')
//             sent.edit(embed)

//             aki.answers.forEach(async (a, i) => {
//                 await sent.react(CHOICE[i])
//             }) 
            
            
//         })
//         .catch(e => {

//         })

// })

module.exports = actions
 