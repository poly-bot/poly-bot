const Bot = require('../api/bot')
const Forum = require('../api/forum')

const actions = new Bot.Actions()

actions.addAction('forum :id', (message, params) => {
    const forum = new Forum(params.id)
    return forum.getQuestionsMessage(message)
})

actions.addAction('question :query', (message, params) => {
    return Forum.AskEmbed(message, params.query)
})

module.exports = actions