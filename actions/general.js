const Discord = require('discord.js')
const Bot = require('../api/bot')
const actions = new Bot.Actions()

const aide = (message, params, events, bot) => {
    const embed = new Discord.MessageEmbed()
    embed.setTitle('Aide')
    embed.setDescription('**Voici ce avec quoi je peux aider**')
    embed.addFields([
        {
            name: 'Stop',
            value: 'Arrête l\'audio\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' stop`'
        },
        {
            name: 'Leave',
            value: 'Leave current voice channel\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' leave`'
        },
        {
            name: 'Question',
            value: 'Posez une question et recevez les meilleurs réponses sur PolyForum.\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' question "MA QUESTION"`'
        },
        {
            name: 'Blague de programmeur',
            value: 'Raconte une blague de programmeur\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' joke`',
        },
        {
            name: 'Tik Tak Toe',
            value: 'Jouer au tik tack toe (niveau: easy, medium, hard, impossible)\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' tik tak toe`'
        },
        {
            name: 'Roche Papier Ciseau',
            value: 'Faite une partie de roche papier ciseau\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' roche papier ciseau`'
        },
        {
            name: 'Cow',
            value: 'Affiche une vache\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' cow`'
        },
        {
            name: 'Scream',
            value: 'HAAAAAAAAAAAAAAAAAAAA\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' scream`'
        },
        {
            name: 'Homeland',
            value: 'Soyuz nerushimyy respublik svobodnykh\n*Utilisation*: `' + bot.settings.trigger + bot.settings.name + ' homeland`'
        }
    ])
    return embed
}
actions.addAction('aide', aide)
actions.addAction('help', aide)

actions.addAction('stop', (message, params, events, bot) => {
    bot.dispatchers.forEach(d => {
        d.destroy()
    })
    bot.dispatchers = new Array
    bot.MusicCollection.clear()

    if(bot.MusicCollection.nowPlayingEmbed != null) {
        try {
            bot.MusicCollection.nowPlayingEmbed.delete()
        } catch(_) {}
        bot.MusicCollection.nowPlayingEmbed = null
    }
})

actions.addAction('leave', (message, params, events, bot) => {
    var c = 0
    bot.client.voice.connections.forEach(connection => {
        connection.disconnect()
        c++
    })

    const embed = new Discord.MessageEmbed()

    if(c > 0) {
        embed.setDescription('Leaving...')
    } else {
        embed.setDescription('No voice channel to leave.')
    }

    return embed
 })

module.exports = actions 