const Discord = require('discord.js')
const Bot = require('../api/bot')
const Music = require('../api/music')
const actions = new Bot.Actions()

const playNext = (voiceChannel, channel, sent, bot) => {
    return new Promise(async (resolve, reject) => {
        const embed = new Discord.MessageEmbed()
        const next = bot.MusicCollection.next()

        if (!next)
            return
        
        if(sent)
            await sent.delete()

        embed.setTitle('Now Playing')
        embed.setDescription(next.title + '\n' + next.artist)
        embed.setThumbnail(next.thumbnail)
        
        const nowPlayingSent = await channel.send(embed)
        if(bot.MusicCollection.nowPlayingEmbed != null) {
            try {
                bot.MusicCollection.nowPlayingEmbed.delete()
            } catch(_) {}
            bot.MusicCollection.nowPlayingEmbed = null
        }
        bot.MusicCollection.nowPlayingEmbed = nowPlayingSent

        voiceChannel.join()
            .then(connection => {
                const dispatcher = connection.play(next.file)

                bot.dispatchers.push(dispatcher)

                const now = Date.now()
                bot.lastPlayed = now

                dispatcher.on('finish', () => {
                    if(bot.MusicCollection.hasNext()) {
                        playNext(voiceChannel, channel, null, bot)
                    } else {
                        bot.MusicCollection.current = null

                        if(bot.MusicCollection.nowPlayingEmbed != null) {
                            try {
                                bot.MusicCollection.nowPlayingEmbed.delete()
                            } catch(_) {}
                            bot.MusicCollection.nowPlayingEmbed = null
                        }

                        setTimeout(() => {
                            if (bot.lastPlayed == now) {
                                connection.disconnect()
                            }
                        }, 2 * 60 * 1000)
                    }
                })

                dispatcher.on('error', console.error)
            })
            .catch(err => {
                embed.setTitle('Music')
                embed.setDescription('Cannot join voice channel')
                channel.send(embed)
            })
    })
}

actions.addAction('play :url', (message, params, events, bot) => { 
    const url = params.url
    const voiceChannel = message.member.voice.channel

    const embed = new Discord.MessageEmbed()

    if (!voiceChannel) {
        embed.setTitle('Music')
        embed.setDescription('You must be in a voice channel to play music.')
        message.channel.send(embed)
        return
    }

    embed.setTitle('Music')
    embed.setDescription('Fetching music...')

    message.channel.send(embed)
        .then(sent => {
            bot.MusicCollection.addToQueue(url)
                .then(music => {
                    if(bot.MusicCollection.current == null) {
                        playNext(voiceChannel, message.channel, sent, bot)
                    } else {
                        embed.setTitle('Added to queue') 
                        embed.setDescription(music.title + '\n' + music.artist)
                        embed.setThumbnail(music.thumbnail)
                        sent.edit(embed)
                    }
                })
                .catch(err => {
                    const errorEmbed = new Discord.MessageEmbed()
                    errorEmbed.setTitle('Music')
                    errorEmbed.setDescription('Cannot play music')
                    message.channel.send(errorEmbed)
                })
        })
})

actions.addAction('next', (message, params, events, bot) => {
    const voiceChannel = message.member.voice.channel

    if(bot.MusicCollection.hasNext())
        playNext(voiceChannel, message.channel, null, bot)
    else {
        bot.dispatchers.forEach(d => {
            d.destroy()
        })
        bot.dispatchers = new Array
        bot.MusicCollection.clear()
    
        if(bot.MusicCollection.nowPlayingEmbed != null) {
            try {
                bot.MusicCollection.nowPlayingEmbed.delete()
            } catch(_) {}
            bot.MusicCollection.nowPlayingEmbed = null
        }
    }
})

module.exports = actions