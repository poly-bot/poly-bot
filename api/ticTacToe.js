const FREE = 0,
    BOT = 1,
    USER = 2

const DIFFICULTY = ['EASY', 'MEDIUM', 'HARD', 'IMPOSSIBLE']

class TicTacToe {
    constructor() {
        this.emoji = {
            '↖️': 0,
            '⬆️': 1,
            '↗️': 2,
            '⬅️': 3,
            '⏺️': 4,
            '➡️': 5,
            '↙️': 6,
            '⬇️': 7,
            '↘️': 8
        }

        this.FREE = FREE
        this.USER = USER
        this.BOT = BOT

        DIFFICULTY.forEach((d, i) => this[d] = i)

        this.botEmoji = '❎'
        this.userEmoji = '🅾️'
        this.winner = null
        this.turn = USER
        this.board = null
        this.initBoard()
        this.difficulty = this.IMPOSSIBLE
    }

    moveToCoordinate(move) {
        const y = Math.floor(move / 3)
        const x = move - (y * 3)
        return {
            x,
            y
        }
    }

    play(move) {
        if (this.turn != USER) return
        const coord = this.moveToCoordinate(move)
        const free = this.isFree(coord)

        if (free) {
            this.board[coord.y][coord.x] = USER
            this.turn = BOT
            return true
        } else {
            return false
        }
    }

    playBot() {
        var done = false
        const a = [BOT]
        if(this.difficulty >= this.MEDIUM) a.push(USER)

        for(var type in a) {
            if(type == 0) type = BOT
            else type = USER
            for(var i = 0; i < 3; ++i) {
                if(this.board[0][i] == type && this.board[1][i] == type && this.board[2][i] == FREE) {
                    return this.board[2][i] = BOT
                } else if(this.board[0][i] == type && this.board[1][i] == FREE && this.board[2][i] == type) {
                    return this.board[1][i] = BOT
                } else if(this.board[0][i] == FREE && this.board[1][i] == type && this.board[2][i] == type) {
                    return this.board[0][i] = BOT
                } else if(this.board[i][0] == type && this.board[i][1] == type && this.board[i][2] == FREE) {
                    return this.board[i][2] = BOT
                } else if(this.board[i][0] == type && this.board[i][1] == FREE && this.board[i][2] == type) {
                    return this.board[i][1] = BOT
                } else if(this.board[i][0] == FREE && this.board[i][1] == type && this.board[i][2] == type) {
                    return this.board[i][0] = BOT
                }
            }

            if(this.board[0][0] == type && this.board[1][1] == type && this.board[2][2] == FREE) {
                return this.board[2][2] = BOT
            }
            if(this.board[0][0] == type && this.board[1][1] == FREE && this.board[2][2] == type) {
                return this.board[1][1] = BOT
            }
            if(this.board[0][0] == FREE && this.board[1][1] == type && this.board[2][2] == type) {
                return this.board[0][0] = BOT
            }
            
            if(this.board[0][2] == type && this.board[1][1] == type && this.board[2][0] == FREE) {
                return this.board[2][0] = BOT
            }
            if(this.board[0][2] == type && this.board[1][1] == FREE && this.board[2][0] == type) {
                return this.board[1][1] = BOT
            }
            if(this.board[0][2] == FREE && this.board[1][1] == type && this.board[2][0] == type) {
                return this.board[0][2] = BOT
            }
        }

        if(this.difficulty >= this.HARD) {
            if(this.difficulty >= this.IMPOSSIBLE) {
                // CORNER STRATEGY
                if(this.board[0][1] == USER && this.board[1][0] == USER && this.board[0][2] == FREE && this.board[2][0] == FREE && this.board[0][0] == FREE) {
                    return this.board[0][0] = BOT
                }
                if(this.board[1][2] == USER && this.board[0][1] == USER && this.board[2][2] == FREE && this.board[0][0] == FREE && this.board[0][2] == FREE) {
                    return this.board[0][2] = BOT
                }
                if(this.board[2][1] == USER && this.board[1][2] == USER && this.board[2][0] == FREE && this.board[0][2] == FREE && this.board[2][2] == FREE) {
                    return this.board[2][2] = BOT
                }
                if(this.board[1][0] == USER && this.board[2][1] == USER && this.board[0][0] == FREE && this.board[2][2] == FREE && this.board[2][0] == FREE) {
                    return this.board[2][0] = BOT
                }
                

                // STRATEGY L
                if(this.board[0][0] == USER && this.board[1][2] == USER && this.board[0][1] == FREE && this.board[1][1] == BOT) {
                    return this.board[0][1] = BOT
                }
                if(this.board[0][0] == USER && this.board[2][1] == USER && this.board[1][0] == FREE && this.board[1][1] == BOT) {
                    return this.board[1][0] = BOT
                }
                if(this.board[0][2] == USER && this.board[1][0] == USER && this.board[0][1] == FREE && this.board[1][1] == BOT) {
                    return this.board[0][1] = BOT
                }
                if(this.board[0][2] == USER && this.board[2][1] == USER && this.board[1][2] == FREE && this.board[1][1] == BOT) {
                    return this.board[1][2] = BOT
                }
                if(this.board[2][0] == USER && this.board[0][1] == USER && this.board[1][0] == FREE && this.board[1][1] == BOT) {
                    return this.board[1][0] = BOT
                }
                if(this.board[2][0] == USER && this.board[1][2] == USER && this.board[2][1] == FREE && this.board[1][1] == BOT) {
                    return this.board[2][1] = BOT
                }
                if(this.board[2][2] == USER && this.board[1][0] == USER && this.board[2][1] == FREE && this.board[1][1] == BOT) {
                    return this.board[2][1] = BOT
                }
                if(this.board[2][2] == USER && this.board[0][1] == USER && this.board[1][2] == FREE && this.board[1][1] == BOT) {
                    return this.board[1][2] = BOT
                }
            }
            

            // If USER PLAY CORNER
            if((this.board[0][0] == USER || this.board[2][0] == USER || this.board[2][2] == USER || this.board[0][2] == USER) && this.board[1][1] == FREE) {
                return this.board[1][1] = BOT
            }

            // IF USER PLAY OUTTER MIDDLE
            if((this.board[0][1] == USER || this.board[1][0] == USER || this.board[1][2] == USER || this.board == [2][1] == USER)) {
                if(this.board[0][1] == USER && this.board[2][1] == FREE) return this.board[2][1] = BOT
                if(this.board[1][0] == USER && this.board[1][2] == FREE) return this.board[1][2] = BOT
                if(this.board[1][2] == USER && this.board[1][0] == FREE) return this.board[1][0] = BOT
                if(this.board[2][1] == USER && this.board[0][1] == FREE) return this.board[0][1] = BOT
                
            }

            // IF USER PLAY MIDDLE
            if(this.board[1][1] == USER) {
                if(this.board[0][0] == FREE) return this.board[0][0] = BOT
                if(this.board[0][2] == FREE) return this.board[0][2] = BOT
                if(this.board[2][0] == FREE) return this.board[2][0] = BOT
                if(this.board[2][2] == FREE) return this.board[2][2] = BOT
            }
        }
        

        while(!done) {
            const x = Math.floor(Math.random() * 3)
            const y = Math.floor(Math.random() * 3)
            if(this.isFree({x, y})) {
                done = true
                this.board[y][x] = BOT
                return
            }
        }
    } 

    isFree(coordinate) {
        return this.board[coordinate.y][coordinate.x] == FREE
    }

    getWinner() {
        for(var i = 0; i < 3; ++i) {
            if((this.board[i][0] == this.board[i][1]) && (this.board[i][0] == this.board[i][2]) && (this.board[i][0] != FREE)) {
                return this.winner = this.board[i][0]
            }
            if((this.board[0][i] == this.board[1][i]) && (this.board[0][i] == this.board[2][i]) && (this.board[0][i] != FREE)) {
                return this.winner = this.board[0][i]
            }
        }
        if((this.board[0][0] == this.board[1][1]) && (this.board[0][0] == this.board[2][2]) && (this.board[0][0] != FREE)) {
            return this.winner = this.board[0][0]
        }
        if((this.board[2][0] == this.board[1][1]) && (this.board[2][0] == this.board[0][2]) && (this.board[2][0] != FREE)) {
            return this.winner = this.board[2][0]
        }

        return null
    }

    getWinnerFromEmoji(emoji) {
        if(emoji == this.botEmoji) return BOT
        else if(emoji == this.userEmoji) return USER
        else return FREE
    }

    getWinnerStr() {
        switch (this.winner) {
            case USER:
                return this.userEmoji + ' a gagné!'
            case BOT:
                return this.botEmoji + ' a gagné!'
            default:
                return 'personne n\'a gagné.'
        }
    }

    boardIsFull() {
        var full = true
        this.board.forEach(line => {
            line.forEach(square => {
                if(square == FREE) full = false
            })
        })
        return full
    }

    initBoard() {
        const board = new Array
        for (var i = 0; i < 3; ++i) {
            board.push(new Array)
            for (var j = 0; j < 3; ++j) {
                board[i].push(FREE)
            }
        }
        this.board = board
    }

    getString() {
        var output = ''
        var i = 0
        try {
            this.board.forEach(line => {
                line.forEach(square => {
                    if (square == FREE) {
                        // output += Object.keys(this.emoji)[i]
                        output += '◻️'
                    } else if (square == USER) {
                        output += this.userEmoji
                    } else if (square == BOT) {
                        output += this.botEmoji
                    }

                    i++
                })
                output += '\n'
            })
        } catch (e) {
            console.error(e)
        }

        return output
    }

    getDifficultyStr(difficulty) {
        return DIFFICULTY[difficulty]
    }
    getDifficultiesStr() {
        var str = ''
        DIFFICULTY.forEach((d, i) => {
            str += d
            if(i < DIFFICULTY.length - 1) str += ', '
        })
        return str
    }
    getDifficulty(str) {
        var index = null
        DIFFICULTY.forEach((diff, i) => {
            if(diff.toLocaleLowerCase() == str.toLocaleLowerCase()) index = i
        })
        return index
    }
}

module.exports = TicTacToe