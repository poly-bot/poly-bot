const request = require('request')
const Bot = require('./bot')

const settings = Bot.Bot.getSettings()
var recent = new Array
const timeLimit = 2 * 24 * 60 * 60 * 1000 // 2 days

class Meme {
    static getRandomSub() {
        const SUBS = settings.meme.subs
        return SUBS[Math.floor(Math.random() * SUBS.length)]
    }

    static fetchMeme() {
        return new Promise((resolve, reject) => {
            const sub = Meme.getRandomSub()
            const limit = 4
            const minKarma = settings.meme.minKarma

            request('http://www.reddit.com/r/' + sub + '/hot.json?limit=' + limit, (error, res, body) => {
                if(error) return reject(error)
                const data = JSON.parse(body)

                Meme.checkRecent()

                var all = new Array
                var memes = new Array
                data.data.children.forEach(c => {
                    try {
                        console.log(c)
                        const title = c.data.title
                        if(!title) return
                        const img = c.data.preview.images[0].source.url.replace('amp;s', 's')
                        if(!img) return
                        const id = c.data.id
                        if(!id) return
                        const date = new Date
                        const score = c.data.score
                        const link = 'http://www.reddit.com' + c.data.permalink
                        const sub = {
                            name: c.data.subreddit,
                            url: 'http://www.reddit.com/r/' + c.data.subreddit
                        }
                        if(score < minKarma) return

                        var already = recent.filter(r => {
                            return r.id
                        })

                        const out = {title, img, id, date, score, link, sub}
                        if(already.length == 0) memes.push(out)
                        all.push({title, img, id, date, score})
                    } catch(e) {console.log(e)}
                })

                if(memes.length == 0) memes = all
                if(memes.length == 0) reject('No memes')
                const meme = memes[Math.floor(Math.random() * memes.length)]
                recent.push({date: meme.date, id: meme.id})
                resolve(meme)
            })
        })
    }

    static checkRecent() {
        const d = new Date
        recent = recent.filter(r => d - r.Date < timeLimit)
    }
}

module.exports = Meme