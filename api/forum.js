const Discord = require('discord.js')
const request = require('request')
const parser = require('node-html-parser')
var stringSimilarity = require("string-similarity")
const Q_and_A = require('./q-and-a')

const FORUMS = [
    {
        slug: 'question-generale',
        name: 'Question générale',
        url: 'https://gopoly.polymtl.ca/mod/forum/view.php?id=12',
        id: 12
    },
    {
        slug: 'planification-rentree',
        name: 'Planification de la rentrée',
        url: 'https://gopoly.polymtl.ca/mod/forum/view.php?id=14',
        id: 14
    },
    {
        slug: 'admission',
        name: 'Admission',
        url: 'https://gopoly.polymtl.ca/mod/forum/view.php?id=15',
        id: 15
    },
    {
        slug: 'etudes-et-inscription-certificats',
        name: 'Études et inscription: Certificats et microprogrammes de 1er cycle',
        url: 'https://gopoly.polymtl.ca/mod/forum/view.php?id=16',
        id: 16
    },
    {
        slug: 'etudes-et-inscription-bac',
        name: 'Études et inscription: Baccalauréat et année préparatoire',
        url: 'https://gopoly.polymtl.ca/mod/forum/view.php?id=17',
        id: 17
    },
    {
        slug: 'etudes-et-inscription-sup',
        name: 'Études et inscription: Cycles supérieurs',
        url: 'https://gopoly.polymtl.ca/mod/forum/view.php?id=18',
        id: 18
    },
    {
        slug: 'vie-etudiante',
        name: 'Vie étudiante',
        url: 'https://gopoly.polymtl.ca/mod/forum/view.php?id=20',
        id: 20
    }
]

const MONTHS = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']

class Forum {
    constructor(slug) {
        this.url = null
        this.name = null
        this.slug = null
        this.id = null

        const forum = FORUMS.filter(f => f.slug == slug || f.name == slug || f.url == slug)

        if (forum.length > 0) {
            this.url = forum[0].url
            this.name = forum[0].name
            this.slug = forum[0].slug
            this.id = forum[0].id
        }
    }

    getQuestions() {
        return new Promise((resolve, reject) => {
            request({
                url: this.url,
                strictSSL: false
            }, async (error, response, body) => {
                if (error) return reject(error)

                const root = parser.parse(body)
                const questionsEl = root.querySelectorAll('.discussion-list tbody tr')
                var questions = new Array

                try {
                    for (const questionEl of questionsEl) {
                        // Each questions
                        const question = {}
                        const itemsEl = questionEl.querySelectorAll('td, th')
                        var i = 0
    
                        for (const item of itemsEl) {
                            const classes = item.attributes.class.split(' ')
                            if (classes.includes('topic')) {
                                const a = item.querySelector('a')
                                const title = a.text.trim()
                                const url = a.attributes.href.trim()
                                try {
                                    const posts = await this.getPostsFromQuestion(url)
                                    question.posts = posts
                                } catch (e) {
                                    console.error(e)
                                    question.posts = new Array
                                }
                                question.title = title
                                question.url = url
    
                            } else if (classes.includes('author')) {
                                const items = item.querySelectorAll('.author-info div')
                                const author = items[0].text
                                const date = new Date(items[1].querySelector('time').attributes['data-timestamp'] * 1000)
                                question.author = author
                                question.date = date
                            } else if (i == 2) {
                                const items = item.querySelectorAll('.author-info div')
                                const author = items[0].text
                                const date = new Date(items[1].querySelector('time').attributes['data-timestamp'] * 1000)
                                question.last = {
                                    author,
                                    date
                                }
                            } else if (i == 3) {
                                const answers = Number(item.text.trim())
                                question.answers = answers
                            }
    
                            i++
                        }
    
                        questions.push(question)
                    }
                } catch(e) {
                    console.error(e)
                }

                questions = questions.sort((a, b) => {
                    const dateA = a.date
                    const dateB = b.date
                    if(dateA > dateB) return -1
                    else return 1
                })
                
                resolve(questions)
            })
        })
    }
    getQuestionsMessage(message, params = {reaction: '👌', max: 5, previewLength: 150}) {
        return new Promise((resolve, reject) => {
            this.getQuestions()
                .then(questions => {
                    const content = new Discord.MessageEmbed()

                    content.setTitle(this.name)
                    content.setURL(this.url)

                    if(questions.length == 0) {
                        content.setDescription('Aucune questions n\'ont été trouvées.')
                        message.react(params.reaction)
                        return resolve(content)
                    }

                    content.setDescription('Voici les dernières questions :')

                    if(questions.length > params.max) questions = questions.slice(0, params.max)
 
                    questions.forEach((question, i) => {
                        const d = question.date.getDay() + ' ' + MONTHS[question.date.getMonth()] + ' ' + question.date.getFullYear()
                        var text = '[' + question.title + '](' + question.url + ')'
                        if(question.posts.length > 0) {
                            var t = question.posts[0].content.trim()
                            if(t.length > params.previewLength) t = t.substr(0, params.previewLength - 3) + '...'
                            text += '```' + t + '```'
                        }
                        if(i < questions.length - 1) text += '\n'

                        content.addField(d, text)
                    })

                    message.react(params.reaction)
                    resolve(content)
                })
                .catch(err => reject(err))
        })
    }

    getPostsFromQuestion(url) {
        return new Promise((resolve, reject) => {
            request({
                url,
                strictSSL: false
            }, (error, response, body) => {
                if (error) return reject(error)
                const qRoot = parser.parse(body)
                const posts = new Array

                qRoot.querySelectorAll('.forumpost').forEach((post, i) => {
                    const content = post.querySelector('.post-content-container')
                    var contentText = content.text
                    while (contentText.includes('\n')) {
                        contentText = contentText.replace('\n', ' ')
                    }
                    while (contentText.includes('  ')) {
                        contentText = contentText.replace('  ', ' ')
                    }

                    const header = post.querySelector('.header')
                    const title = header.querySelector('h3').text
                    const author = header.querySelector('.mb-3 a')
                    const date = new Date(header.querySelector('time').attributes['datetime'])

                    posts.push({
                        content: contentText,
                        html: content.innerHTML,
                        date: date,
                        title: title,
                        author: {
                            name: author.text.trim(),
                            url: author.attributes.href
                        }
                    })
                })

                resolve(posts)
            })
        })
    }

    static getAllQuestions() {
        return new Promise((resolve, reject) => {
            var fetched = 0
            var questions = new Array

            FORUMS.forEach(f => {
                const forum = new Forum(f.slug)
                forum.getQuestions()
                    .then(q => {
                        q.forEach(question => {
                            question.forum = f.slug
                            questions.push(question)
                        })
                        fetched++
                        onFetch()
                    })
                    .catch(e => {
                        console.error(e)
                        fetched++
                        onFetch()
                    })
            })

            const onFetch = () => {
                if(fetched != FORUMS.length) return

                questions = questions.sort((a, b) => {
                    const dateA = a.date
                    const dateB = b.date
                    if(dateA > dateB) return -1
                    else return 1
                })

                resolve(questions)
            }
        })
    }

    static ask(query, params = {max: 4, minRating: 0.2, contentRatingMultiplier: 1.2}) {
        return new Promise((resolve, reject) => {
            Forum.getAllQuestions()
                .then(questions => {
                    var answers = new Array

                    questions.forEach(question => {
                        const titleRating = stringSimilarity.compareTwoStrings(question.title, query)
                        var contentRating = 0

                        // if(question.posts.length > 0) {
                        //     contentRating = stringSimilarity.compareTwoStrings(question.posts[0].content, query)
                        // }

                        const rating = titleRating + contentRating * params.contentRatingMultiplier
                        answers.push({question, rating})
                    })

                    answers = answers.filter(a => a.rating > params.minRating)
                    answers = answers.sort((a, b) => {
                        if(a.rating > b.rating) return -1
                        else return 1
                    })
                    if(answers.length > params.max) answers = answers.slice(0, params.max)
                    
                    const output = new Array
                    answers.forEach(a => output.push(a.question))
                    resolve(output)
                })
                .catch(e => {
                    reject(e)
                })
        })
    }

    static AskEmbed(message, query, params = {max: 4, minRating: 0.2, contentRatingMultiplier: 3, reaction: '👌', previewLength: 150}) {
        return new Promise((resolve, reject) => {
            var qAndAFetched = false, forumFetched = false
            var qAndAEmbed = null, forumEmbed = null

            Q_and_A.search(query)
                .then(qAndA => {
                    qAndAFetched = true

                    if(qAndA.length == 0) {
                        onResult()
                        return
                    }

                    const embed = new Discord.MessageEmbed()
                    embed.setTitle('Résultats de recherche dans le Q&A')
                    embed.setDescription('Voici les meilleurs résultats de Q&A pour \"' + query + '\"')

                    qAndA.forEach(item => {
                        embed.addField(item.question[0], item.answer)
                    })

                    qAndAEmbed = embed
                    onResult()
                })
                .catch(e => {
                    console.error(e)
                    qAndAFetched = true
                    onResult()
                })
            console.log(query)
            Forum.ask(query, params)
                .then(answers => {
                    forumFetched = true

                    if(answers.length == 0) {
                        onResult()
                        return 
                    }

                    const embed = new Discord.MessageEmbed()
                    embed.setTitle('Résultats de recherche dans PolyForum')
                    embed.setDescription('Voici les meilleurs résultats de questions pour \"' + query + '\"')

                    answers.forEach((answer, i) => {
                        var text = ''
                        const forum = FORUMS.filter(f => f.slug == answer.forum)
                        if(forum.length > 0) {
                            const name = forum[0].name
                            const url = forum[0].url
                            text += '**[' + name + '](' + url + ')** : '
                        }

                        const d = answer.date.getDay() + ' ' + MONTHS[answer.date.getMonth()] + ' ' + answer.date.getFullYear()
                        text += '[' + answer.title + '](' + answer.url + ')'
                        if(answer.posts.length > 0) {
                            var t = answer.posts[0].content.trim()
                            if(t.length > params.previewLength) t = t.substr(0, params.previewLength - 3) + '...'
                            text += '```' + t + '```'
                        }
                        if(i < answers.length - 1) text += '\n'

                        embed.addField(d, text)
                    })

                    forumEmbed = embed
                    onResult()
                })
                .catch(e => {
                    console.error(e)
                    forumFetched = true
                    onResult()
                })

            const onResult = () => {
                if(!forumFetched || !qAndAFetched) return

                message.react(params.reaction)
                
                if(!forumEmbed && !qAndAEmbed) {
                    const embed = new Discord.MessageEmbed()
                    embed.setDescription('Aucun résultat n\'a été trouvé pour \"' + query + '\"')
                    embed.setColor('#ff6347')
                    message.channel.send(embed)

                    return resolve(null)
                }

                if(qAndAEmbed) message.channel.send(qAndAEmbed)
                if(forumEmbed) message.channel.send(forumEmbed)
                
                resolve(null)
            }
        })
    }
}

module.exports = Forum