const request = require('request')
const parser = require('node-html-parser')

const baseUrl = 'https://justscream.baby/listen'
const maxPage = 509
const maxDuration = 5

const EXCEPTS = [999999, 6224]

class Scream {
    static getScreams(playlist = 'all', page = 1) {
        return new Promise((resolve, reject) => {
            const url = baseUrl + '/' + playlist + '/' + page

            request({url, strictSSL: false}, (error, reject, body) => {
                if(error) return reject(error)

                const root = parser.parse(body)
                const items = root.querySelectorAll('#playlist .track')
                
                const screams = new Array
                items.forEach(item => {
                    const filename = item.attributes['data-filename']
                    const trackname = item.querySelector('.trackname').text
                    id = Number(trackname.replace('Scream #', ''))
                    const duration = Number(item.querySelector('.duration').text.replace('s', ''))
                    
                    if(Number.isNaN(id)) return
                    if(EXCEPTS.includes(id)) return
                    if(duration > maxDuration) return

                    screams.push({
                        filename, 
                        name: trackname, 
                        id,
                        duration,
                        url: 'https://justscream.baby/' + id,
                        fileUrl: 'https://justscream.baby/screams/' + filename
                    })
                })

                resolve(screams)
            })
        })
    }

    static getRandomScream(playlist = 'all', rejectPlaylists = ['hope', 'hello2021']) {
        return new Promise((resolve, reject) => {
            const page = Math.floor(Math.random() * (maxPage - 1)) + 1
            const url = baseUrl + '/' + playlist + '/' + page

            request({url, strictSSL: false}, (error, reject, body) => {
                if(error) return reject(error)

                const root = parser.parse(body)
                const items = root.querySelectorAll('#playlist .track')

                const rejected = new Array

                rejectPlaylists.forEach(async (playlist) => {
                    try {
                        const screams = await Scream.getScreams(playlist, 1)
                        screams.forEach(scream => rejected.push(screams.id))
                    } catch(e) {}
                })
                
                var scream = {}
                var id = -1
                var duration = 0
                do {
                    const item = items[Math.floor(Math.random() * items.length)]
                    const filename = item.attributes['data-filename']
                    const trackname = item.querySelector('.trackname').text
                    id = Number(trackname.replace('Scream #', ''))
                    duration = Number(item.querySelector('.duration').text.replace('s', ''))
                    
                    scream = {
                        filename, 
                        name: trackname, 
                        id,
                        duration,
                        url: 'https://justscream.baby/' + id,
                        fileUrl: 'https://justscream.baby/screams/' + filename
                    }
                } while(id == -1 || Number.isNaN(id) || EXCEPTS.includes(id) || rejected.includes(id) || duration > maxDuration)

                resolve(scream)
            })
        })
    }
}

module.exports = Scream