const fs = require('fs')
const isAUrl = require('is-url')
const urlParse = require('url-parse')
const queryString = require('query-string')
const YoutubeMp3Downloader = require("youtube-mp3-downloader")

const MP3_PATH = '/Users/cfstcyr/Desktop/music'
const FFMPEG_PATH = '/usr/local/Cellar/ffmpeg/4.4_2/bin/ffmpeg'
const VALID_HOSTNAMES = {youtube: ['www.youtube.com', 'youtu.be']}

var allValidHostnames = new Array
Object.keys(VALID_HOSTNAMES).forEach(k => VALID_HOSTNAMES[k].forEach(v => allValidHostnames.push(v)))

if(!fs.lstatSync(MP3_PATH).isDirectory())
    throw new Error('Invalid path for mp3')

class MusicCollection {
    constructor(bot) {
        this.collection = new Array
        this.current = null
        this.bot = bot
    }

    next() {
        const music = this.collection.shift()
        this.current = music
        return music
    }

    clear() {
        this.collection = new Array
        this.current = null
    }

    hasNext() {
        return this.collection.length > 0
    }

    addToQueue(url) {
        return new Promise((resolve, reject) => {
            const urlData = MusicCollection.parseURL(url)
            const infoFilePath = `${MP3_PATH}/${urlData.id}.txt`

            if(fs.existsSync(infoFilePath)) {
                // Song already downloaded
                const music = JSON.parse(fs.readFileSync(infoFilePath))
                this.collection.push(music)
                resolve(music)
            } else {
                // Song to download
                MusicCollection.downloadVideo(urlData.id)
                    .then(video => {
                        fs.writeFileSync(infoFilePath, JSON.stringify(video))
                        this.collection.push(video)
                        resolve(video)
                    })
                    .catch(err => reject(err))
            }
        })
    }

    static downloadVideo(youtubeID) {
        return new Promise((resolve, reject) => {
            const YD = new YoutubeMp3Downloader({
                'ffmpegPath': FFMPEG_PATH,
                'outputPath': MP3_PATH
            })
    
            YD.download(youtubeID, `${youtubeID}.mp3`)

            YD.on('error', (error, data) => reject(error))
            YD.on('finished', (error, data) => resolve(data))
        })
    }

    static parseURL(url) {
        if(!isAUrl(url))
            throw new Error('Invalid URL')
        
        const urlData = urlParse(url)

        if(!allValidHostnames.includes(urlData.hostname))
            throw new Error('Invalid hostname')
        
        if(VALID_HOSTNAMES.youtube.includes(urlData.hostname))
            return MusicCollection.parseYoutubeURL(urlData)
    }

    static parseYoutubeURL(urlData) {
        var url, id

        url = urlData.href
        
        if(urlData.hostname == 'www.youtube.com') {
            const query = queryString.parse(urlData.query)
            id = query.v
        } else if(urlData.hostname == 'youtu.be') {
            id = urlData.pathname.replace('/', '')
        } else {
            throw new Error('Invalid hostname for Youtube')
        }

        return {url, id}
    }
}

module.exports = MusicCollection