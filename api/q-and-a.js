const request = require('request')
const stringSimilarity = require("string-similarity")

class Q_and_A {
    static getQAndA() {
        return new Promise((resolve, reject) => {
            const url = 'http://192.168.2.142:9830/poly-bot/api/q-and-a'
            request(url, (error, response, body) => {
                if(error) return reject(error)
                try {
                    const data = JSON.parse(body)
                    const qAndA = new Array
                    data.forEach(element => {
                        qAndA.push({question: element.question, answer: element.answer})
                    })
                    resolve(qAndA)
                } catch(e) {
                    reject(e)
                }
            })
        })
    }

    static search(query, params = {minRating: 0.2, max: 5}) {
        return new Promise((resolve, reject) => {
            Q_and_A.getQAndA()
                .then(res => {
                    var qAndA = new Array
                    res.forEach(item => {
                        const similarity = stringSimilarity.findBestMatch(query, item.question)
                        qAndA.push({
                            question: item.question,
                            answer: item.answer,
                            bestMatch: similarity.bestMatch.target,
                            rating: similarity.bestMatch.rating
                        })
                    })
                    
                    qAndA = qAndA.filter(item => item.rating > params.minRating)
                    qAndA = qAndA.sort((a, b) => {
                        if(a.rating > b.rating) return -1
                        else return 1
                    })
                    if(qAndA.length > params.max) qAndA = qAndA.slice(0, params.max)

                    resolve(qAndA)
                })
                .catch(e => {
                    reject(e)
                })
        })
        
    }
}

module.exports = Q_and_A