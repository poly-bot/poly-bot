const path = require('path')
const events = require('events')
const Discord = require('discord.js')
const fs = require('fs')
const Forum = require('./forum')
const MusicCollection = require('./music')


const NSFWChannels = ['channel-2']

const splitQuery = (query, splitter = ' ') => {
    const count = {
        '"': 0, '`': 0
    }
    const allClosed = () => {
        var closed = true
        Object.keys(count).forEach(k => {
            if(count[k] != 0) closed = false
        })
        return closed
    }
    var output = new Array
    var lastSplit = 0

    query.split('').forEach((char, i) => {
        if(Object.keys(count).includes(char)) {
            count[char] = (count[char] + 1) % 2
        } else if(char == splitter && allClosed()) {
            output.push(query.substring(lastSplit, i))
            lastSplit = i
        }
    })

    output.push(query.substr(lastSplit))

    output.forEach((o, i) => {
        Object.keys(count).forEach(del => {
            while(o.includes(del)) {
                o = o.replace(del, '')
            }
        })
        output[i] = o.trim()
    })
    
    if(!allClosed) throw new Error('Left open')
    return output
}

class Bot {
    constructor(token) {
        if(!token) throw new Error('Bot must have a token')
        this.token = token
        this.ready = false

        this.lastPlayed = null
        this.musicQueue = new Array
        this.isPlaying = false

        this.bot_channel = null
        this.question_channel = null
        this.NSFWChannels = new Array

        this.actions = new Array

        this.MusicCollection = new MusicCollection()

        this.settings = Bot.getSettings()

        this.client = new Discord.Client()

        this.dispatchers = new Array

        this.client.on('ready', () => {this.ready = true})
        this.client.on('message', msg => {
            const msgFromBot = msg.author.id == this.client.user.id

            if(!msgFromBot) this.analyseMessage(msg)
 
            const message = msg.content
            const words = message.split(' ')
            if(words.length > 0) {
                const isInBotChannel = this.bot_channel ? (msg.channel.name == this.bot_channel || msg.channel.id == this.bot_channel) && !msgFromBot : false
                const isInQuestionChannel = this.question_channel ? (msg.channel.name == this.question_channel || msg.channel.id == this.question_channel) && !msgFromBot : false

                if(isInQuestionChannel) {
                    Forum.AskEmbed(msg, msg.content)
                        .then(answer => {
                            if(answer) {
                                msg.channel.send(answer)
                            } else {
                                console.log('Nothing sent')
                            }
                        })
                        .catch(e => {
                            console.log(e)
                        })
                } else if(words[0] == this.settings.trigger + this.settings.name || isInBotChannel) {
                    words.shift()
                    const query = isInBotChannel ? message : words.join(' ')

                    this.answer(msg, query)
                        .then(({answer, events}) => {
                            if(answer){
                                msg.channel.send(answer)
                                    .then(sentMessage => {
                                        events.emit('sent', sentMessage)
                                    })
                                    .catch(e => {
                                        events.emit('error', e)
                                    })
                            } else {
                                console.log('Nothing sent')
                                events.emit('sent', null)
                            }
                        })
                        .catch(({error, events}) => { 
                            console.error(error)
                            const embed = new Discord.MessageEmbed()
                            embed.setColor(this.settings.colors.error)
                            embed.setDescription('Whoops, je ne comprend pas ce que tu recherches.')
                            msg.channel.send(embed)
                        })
                }
            }
        })
        this.client.login(this.token)
        
    }

    static getSettings() {
        try {
            return JSON.parse(fs.readFileSync('./bot-settings.json'))
        } catch(e) {
            console.log(e)
            return {}
        }
    }

    addAction(path, action) {
        if(typeof(path) != typeof('')) throw new Error('Path must be a string')
        if(typeof(action) != typeof(() => {})) throw new Error('Action must be a function')
        this.actions.push({path, action})
    }

    addActions(actions) {
        if(typeof(actions) != typeof(new Actions)) throw new Error('Must be a Action class')
        actions.actions.forEach(action => {
            this.addAction(action.path, action.action)
        })
    }

    _getAction(message, query, events) {
        var qWords = splitQuery(query)

        var act = null
        var found = false
        this.actions.forEach(action => {
            if(found) return
            
            var kWords = splitQuery(action.path)

            qWords = qWords.filter(q => q != '')
            kWords = kWords.filter(q => q != '')
            
            if(kWords.length != qWords.length) return

            var ok = true
            const params = {}
            kWords.forEach((kWord, i) => {
                if(kWord.charAt(0) == ':') {
                    params[kWord.replace(':', '')] = qWords[i]
                } else {
                    if(kWord.toLowerCase().trim() != qWords[i].toLowerCase().trim()) ok = false
                }
            })
            if(ok) {
                act = action.action(message, params, events, this)
                found = true
            }
        })

        if(!found) {
            const embed = new Discord.MessageEmbed()
            embed.setColor(this.settings.colors.error)
            embed.setDescription('Désolé, je ne trouve pas ce que vous cherchez. Essayez `' + this.settings.trigger + this.settings.name + ' aide` pour savoir ce que je peux faire.')
            return embed
        }
        return act
    }

    answer(message, query) {
        return new Promise((resolve, reject) => {
            const eventEmitter = new events.EventEmitter()
            const action = this._getAction(message, query, eventEmitter, this)
            if(!action) {
                resolve({
                    answer: null,
                    events: eventEmitter
                })
            }

            if(action.then || action.catch) {
                action
                    .then(res => {
                        resolve({
                            answer: res,
                            events: eventEmitter
                        })
                    })
                    .catch(err => {
                        reject({
                            error: err,
                            events: eventEmitter
                        })
                    })
            } else if(action != null) {
                resolve({
                    answer: action,
                    events: eventEmitter
                })
            } else {
                resolve({
                    answer: null,
                    events: eventEmitter
                })
            }
        })
    }
}

Bot.prototype.analyseMessage = (message) => {

}

class Actions {
    constructor() {
        this.actions = new Array
    }

    addAction(path, action) {
        if(typeof(path) != typeof('')) throw new Error('Path must be a string')
        if(typeof(action) != typeof(() => {})) throw new Error('Action must be a function')
        this.actions.push({path, action})
    }
}

module.exports = {Bot, Actions}