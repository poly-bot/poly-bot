const DIFFICULTY = ['EASY', 'MEDIUM', 'HARD']

class Demineur {
    constructor(difficulty = 'MEDIUM', x = 10, y = 20) {
        this.BOMB = 'B'
        this.FREE = ' '

        DIFFICULTY.forEach(d => this[d] = d)

        this.x = x
        this.y = y

        if(!DIFFICULTY.includes(difficulty)) {
            throw new Error('Difficulty invalid')
        }
        this.difficulty = difficulty

        this.board = null

        this.resetBoard()
        this.completeBoard()
    }

    resetBoard() {
        this.board = new Array

        for(var i = 0; i < this.y; ++i) {
            this.board.push(new Array)
            for(var j = 0; j < this.x; ++j) {
                this.board[i].push(this.FREE)
            }
        }
    }

    completeBoard() {
        var nthBombs = 0
        const size = this.x * this.y

        switch(this.difficulty) {
            case 'EASY':
                nthBombs = size * 0.05
                break;
            case 'MEDIUM':
                nthBombs = size * 0.1
                break;
            case 'HARD':
                nthBombs = size * 0.2
                break;
            default:
                nthBombs = size * 0.1
                break;
        }

        var count = 0
        do {
            const x = Math.floor(Math.random() * this.x)
            const y = Math.floor(Math.random() * this.y)

            if(this.board[y][x] == this.FREE) {
                this.board[y][x] = this.BOMB
                count++
            }
        } while(count < nthBombs)

        for(var y = 0; y < this.y; ++y) {
            for(var x = 0; x < this.x; ++x) {
                if(this.board[y][x] == this.FREE) {
                    const neighbors = this.getNeighbors(x, y)
                    const bombs = neighbors.filter(c => c == this.BOMB)
                    if(bombs.length > 0) {
                        this.board[y][x] = bombs.length
                    }
                }
            }
        }
    }

    getNeighbors(x, y) {
        const neighbors = new Array

        try {
            neighbors.push(this.board[y][x - 1])
        } catch(e) {}
        try {
            neighbors.push(this.board[y - 1][x - 1])
        } catch(e) {}
        try {
            neighbors.push(this.board[y + 1][x - 1])
        } catch(e) {}
        try {
            neighbors.push(this.board[y][x + 1])
        } catch(e) {}
        try {
            neighbors.push(this.board[y - 1][x + 1])
        } catch(e) {}
        try {
            neighbors.push(this.board[y + 1][x + 1])
        } catch(e) {}
        try {
            neighbors.push(this.board[y - 1][x])
        } catch(e) {}
        try {
            neighbors.push(this.board[y + 1][x])
        } catch(e) {}

        return neighbors
    }

    log() {
        var str = ''
        for(var y = 0; y < this.y; ++y) {
            str += '|'
            for(var x = 0; x < this.x; ++x) {
                str += this.board[y][x]
            }
            str += '|\n'
        }
        console.log(str)
    }
}

module.exports = Demineur